package com.testing;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class LibraryServiceTest {

    @Mock
    private LibraryService service;

    @Mock
    private LibraryService.DAO dao;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testBasicMethodVerification() {
        final LibraryService service = new LibraryService(dao);

        service.hasBookWithId(42);

        verify(dao).fetchBookById(anyInt());
    }

    @Test
    void testMethodVerificationUsingLambdas() {
        final LibraryService service = new LibraryService(dao);

        service.hasBookWithId(42);

        verify(dao).fetchBookById(argThat(argument -> argument.equals(42)));
    }

    @Test
    void textMethodVerificationUsingLogicalOp() {
        final LibraryService service = new LibraryService(dao);

        service.hasBookWithId(42);

        verify(dao).fetchBookById(eq(42));
    }

    @Test
    void testArgMatcherWithNull() {
        when(service.hasBookWithId(isNull())).thenThrow(IllegalArgumentException.class);

        final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            service.hasBookWithId(null);
        });
        assertThat(exception.getClass(), is(equalTo(IllegalArgumentException.class)));
    }

    @Test
    void testArgMatchersAllArgs() {
        final String author = "Joshua Block";

        when(service.hasBookWithTopicAndAuthor(any(Topic.class), eq(author))).thenReturn(true);

        assertTrue(service.hasBookWithTopicAndAuthor(Topic.COMPUTING, author));
    }

    @Test
    void testMethod() {
        service.hasBookWithId(42);

        verify(service).hasBookWithId(argThat(isValid()));
    }

    @Test
    void testArgumentCaptor() {
        final ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);

        service.hasBookWithId(42);

        verify(service).hasBookWithId(captor.capture());

        assertThat(captor.getValue(), is(equalTo(42)));
    }

    private MyArgMatcher isValid() {
        return new MyArgMatcher();
    }

    public static class MyArgMatcher implements ArgumentMatcher<Integer> {

        Integer integer;

        @Override
        public boolean matches(Integer argument) {
            this.integer = argument;
            return argument != null && argument > 0;
        }

        @Override
        public String toString() {
            return String.format("%d must be a positive integer", integer);
        }
    }

}