package com.testing;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UtilsTest {

    @Test
    void testMethod() {
        final Utils utils = mock(Utils.class);
        List<Book> expect = Arrays.asList(Book.builder()
                .title("Effective Java")
                .pages(250)
                .topic(Topic.COMPUTING)
                .releaseYear(2001)
                .author("Joshua Block")
                .build());
        when(utils.parseLibraryFrom(any(Path.class))).thenReturn(expect);
    }

    @Test
    void testStubbingOfException() {
        final Utils utils = mock(Utils.class);

        when(utils.getBook(anyString())).thenThrow(RuntimeException.class);

        final RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> {
            utils.getBook("");
        });
        assertThat(runtimeException.getClass(), is(equalTo(RuntimeException.class)));
    }
}