package com.testing;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import com.testing.LibraryService.DAO;

class GenericServiceTest {

    @Mock
    private LibraryService.DAO dao;

//    @Spy
//    private LibraryService.DAO dao;

    @Mock
    private LoggerService loggerService;

    @InjectMocks
    private GenericService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testNumberOfInvocations() {
        when(dao.fetchBookById(anyInt())).thenReturn(LibraryService.DEFAULT_BOOK);

        service.fetchBokById(42);

        // Equivalent
        verify(dao).fetchBookById(42);
        verify(dao, times(1)).fetchBookById(42);
        verify(dao, atLeast(1)).fetchBookById(42);
        verify(dao, atLeastOnce()).fetchBookById(42);
        verify(dao, atMost(1)).fetchBookById(42);
        verify(dao, atMostOnce()).fetchBookById(42);
    }

    @Test
    void testSpy() {
        doReturn(null).when(dao).fetchBookById(anyInt());

        Book book = service.fetchBook(42, "Effective Java");

        assertThat(book, is(equalTo(LibraryService.DEFAULT_BOOK)));

        verify(dao).fetchBookByTitle(anyString());
    }

    @Test
    void testInvocationOrder() {
        when(dao.fetchBookById(anyInt())).thenReturn(LibraryService.DEFAULT_BOOK);

        service.fetchBokById(1);
        service.fetchBokById(2);

        InOrder inOrder = inOrder(dao);
        inOrder.verify(dao).fetchBookById(1);
        inOrder.verify(dao).fetchBookById(2);
    }

    @Test
    void testDoRealCallMethod() {

        doCallRealMethod().when(dao).fetchBookById(anyInt());

        service.fetchBokById(42);

    }

    @Test
    void testDoNothing() {
        doNothing()
            .doThrow(RuntimeException.class)
            .when(dao).foo();

        dao.foo();

        final RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> {
            dao.foo();
        });
        assertThat(runtimeException.getClass(), is(equalTo(RuntimeException.class)));
    }

    @Test
    void testMethod() {

        when(dao.fetchBookById(42)).thenReturn(LibraryService.DEFAULT_BOOK);

        Book book = service.fetchBokById(42);

        assertThat(book, is(equalTo(LibraryService.DEFAULT_BOOK)));

        verify(loggerService).log(anyString());
    }

    @Test
    void testDoAnswer() {

        doAnswer(invocation -> {
            Integer id = invocation.getArgument(0, Integer.class);
            return id == 42 ? LibraryService.DEFAULT_BOOK : null;
        }).when(dao).fetchBookById(anyInt());

        Book book = service.fetchBokById(42);

        assertThat(book, is(equalTo(LibraryService.DEFAULT_BOOK)));

    }

    @Test
    void testDoReturn() {

        final GenericService spy = spy(service);

        doReturn(LibraryService.DEFAULT_BOOK).when(spy).fetchBokById(42);

    }

    @Test
    void testDoThrow() {

        doThrow(RuntimeException.class).when(dao).foo();

        final RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> {
            service.fetchBokById(42);
        });
        assertThat(runtimeException.getClass(), is(equalTo(RuntimeException.class)));

    }
}