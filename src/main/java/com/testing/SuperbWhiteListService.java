package com.testing;

import static java.util.stream.Collectors.toSet;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.testing.IsbnSource.Status;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class SuperbWhiteListService {

    private final IsbnRepository isbnRepository;

    private final WhiteListService whiteListService;

    void updateWhiteList(Set<String> isbnSet) {
        Set<String> normalizedIsbnSet = isbnSet.stream()
            .map(isbn -> isbn.replace("-", ""))
            .collect(toSet());
        findNewIsbnAndSave(isbnSet);
        updateStatusToInactive(normalizedIsbnSet);
    }

    private void updateStatusToInactive(Set<String> normalizedIsbnSet) {
        Set<IsbnSource> isbnToChangOnInactive = isbnRepository.receiveIsbnSetThatNotMatchCollection(normalizedIsbnSet);
        isbnToChangOnInactive.forEach(source -> source.setStatus(Status.INACTIVE));
        isbnRepository.saveAll(isbnToChangOnInactive);
    }

    private void findNewIsbnAndSave(Set<String> isbnSet) {
        Set<String> normalizedIsbnSet = isbnSet.stream()
            .map(isbn -> isbn.replace("-", ""))
            .collect(toSet());
        Set<IsbnSource> isbnSourcesThatMatch = isbnRepository.receiveIsbnSetThatMatchCollection(normalizedIsbnSet);
        whiteListService.findNewIsbnAndSaveToWhiteList(isbnSet, isbnSourcesThatMatch);
    }
}
