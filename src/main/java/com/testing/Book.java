package com.testing;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
class Book {
    private String title;
    private int pages;
    private Topic topic;
    private int releaseYear;
    private String author;
}
