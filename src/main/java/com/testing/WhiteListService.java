package com.testing;

import static java.util.stream.Collectors.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.testing.IsbnSource.Status;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class WhiteListService {

    private final IsbnRepository isbnRepository;

    void updateWhiteList(Set<String> isbnSet) {
        //https://stackoverflow.com/questions/1009706/postgresql-max-number-of-parameters-in-in-clause
        List<List<String>> partition = Lists.partition(new ArrayList<>(isbnSet), 2);
        for (List<String> list : partition) {
            Set<String> partOfIsbn = new HashSet<>(list);
            Set<String> partOfNormalizedIsbn = partOfIsbn.stream()
                .map(isbn -> isbn.replace("-", ""))
                .collect(toSet());
            Set<IsbnSource> isbnSourcesThatMatch = isbnRepository.receiveIsbnSetThatMatchCollection(partOfNormalizedIsbn);
            //update statusu dla isbn ktore maczuja bo moga miec ustawione inactive
            updateStatusToActive(isbnSourcesThatMatch);
            findNewIsbnAndSaveToWhiteList(partOfIsbn, isbnSourcesThatMatch);
        }
    }

    void findNewIsbnAndSaveToWhiteList(Set<String> isbnSet, Set<IsbnSource> isbnSourcesThatMatch) {
        Set<String> isbnThatMatch = isbnSourcesThatMatch.stream()
            .map(IsbnSource::getIsbn)
            .collect(toSet());
        SetView<String> difference = Sets.difference(isbnSet, isbnThatMatch);
        Set<IsbnSource> isbnSourceToSave = mapToIsbnSource(difference.immutableCopy());
        isbnRepository.saveAll(isbnSourceToSave);
    }

    private void updateStatusToActive(Set<IsbnSource> isbnSourcesThatMatch) {
        if (!isbnSourcesThatMatch.isEmpty()) {
            Set<String> inactiveIsbnSet = isbnSourcesThatMatch.stream()
                .filter(source -> source.getStatus().equals(Status.INACTIVE))
                .map(IsbnSource::getIsbn)
                .collect(toSet());
            isbnRepository.updateIsbnStatusesThatMatchCollection(Status.ACTIVE, inactiveIsbnSet);
        }
    }

    private Set<IsbnSource> mapToIsbnSource(ImmutableSet<String> immutableCopy) {
        return immutableCopy.stream()
            .map(isbn -> {
                IsbnSource isbnSource = new IsbnSource(isbn);
                isbnSource.setNormalizedIsbn(isbn.replace("-", ""));
                isbnSource.setStatus(Status.ACTIVE);
                return isbnSource;
            })
            .collect(toSet());
    }

}
