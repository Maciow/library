package com.testing;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class LibraryService {

    static Book DEFAULT_BOOK = new Book("Effective Java", 280, Topic.COMPUTING, 2000, "Joshua Block");

    private final DAO dao;

    public boolean hasBookWithId(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("id must be a positive integer");
        }
        return dao.fetchBookById(id) != null;
    }

    public boolean hasBookWithTopicAndAuthor(Topic topic, String author) {
        return true;
    }

    public static class DAO {

        Book fetchBookById(int id) {
            return DEFAULT_BOOK;
        }

        Book fetchBookByTitle(String title) {
            return DEFAULT_BOOK;
        }

        void foo() {
            System.out.println("DAO.foo");
        }
    }
}
