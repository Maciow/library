package com.testing;

import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        Set<String> isbnSet = Set.of(
            "11-1111-1111",
            "22-2222-2222",
            "3-33333-3333333",
            "4-44444-4444444",
            "5-55555-5555555"
        );
        Set<String> isbnSet2 = Set.of(
            "11-1111-1111",
            "22-2222-2222",
            "3-33333-3333333",
            "4-44444-4444444",
            "5-55555-5555555",
            "6-55555-66666666"
        );
//        context.getBean(WhiteListService.class).updateWhiteList(isbnSet2);
//        context.getBean(WhiteListService.class).updateWhiteList(isbnSet);
        context.getBean(SuperbWhiteListService.class).updateWhiteList(isbnSet2);
        context.getBean(SuperbWhiteListService.class).updateWhiteList(isbnSet);
    }

}
