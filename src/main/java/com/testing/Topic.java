package com.testing;

public enum Topic {
    COMPUTING, SCI_FI, FINANCE, NOVEL;
}
