package com.testing;

import java.util.Collection;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.testing.IsbnSource.Status;

interface IsbnRepository extends JpaRepository<IsbnSource, Long> {

    @Query("SELECT i FROM IsbnSource i WHERE i.normalizedIsbn in :collection")
    Set<IsbnSource> receiveIsbnSetThatMatchCollection(@Param("collection") Collection<String> normalizedIsbnSet);

    @Query("SELECT i FROM IsbnSource i WHERE i.normalizedIsbn not in :collection")
    Set<IsbnSource> receiveIsbnSetThatNotMatchCollection(@Param("collection") Collection<String> normalizedIsbnSet);

    @Modifying
    @Transactional
    @Query("update IsbnSource i set i.status=:status where i.isbn in :collection")
    void updateIsbnStatusesThatMatchCollection(@Param("status") Status status, @Param("collection") Collection<String> isbnSet);
}
