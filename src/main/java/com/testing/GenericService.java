package com.testing;

import com.testing.LibraryService.DAO;

class GenericService {

    private LibraryService.DAO dao;
    private LoggerService loggerService;

    public Book fetchBokById(Integer id) {
        final Book book = dao.fetchBookById(id);
        loggerService.log(book.getTitle());
        return book;
    }

    public Book fetchBook(Integer id, String title) {

        Book book = dao.fetchBookById(id);
        if (book == null) {
            book = dao.fetchBookByTitle(title);
        }

        loggerService.log(book.getTitle());
        return book;
    }

}
