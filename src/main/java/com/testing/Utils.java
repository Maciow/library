package com.testing;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

class Utils {
    private static final int INVALID_CSV_TOKEN_ERROR_CODE = 1;
    private static final int MISSING_CSV_FILE = 2;

    List<Book> parseLibraryFrom(Path path) {
        List<Book> library = new ArrayList<>();
        try(BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {
                library.add(getBook(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return library;
    }

    Book getBook(String line) {
        final String[] tokens = line.split(",");
        return Book.builder()
                .title(getToken(tokens, 0))
                .pages(Integer.parseInt(getToken(tokens,1)))
                .topic(Topic.valueOf(getToken(tokens, 2)))
                .releaseYear(Integer.valueOf(getToken(tokens, 3)))
                .author(getToken(tokens, 4))
                .build();
    }

    String getToken(String[] tokens, int idx) {
        final String token = tokens[idx];
        if (token == null) {
            throw new IllegalArgumentException(String.format("%d | Found invalid token at index i:%d", INVALID_CSV_TOKEN_ERROR_CODE, idx));
        }
        return token.trim();
    }
}
