package com.testing;

import java.sql.Timestamp;
import java.time.Instant;

import javax.persistence.*;

import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
class IsbnSource {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String isbn;
    private String normalizedIsbn;
    private Timestamp timestamp = Timestamp.from(Instant.now());
    @Enumerated(EnumType.STRING)
    private Status status;

    enum Status {
        ACTIVE, INACTIVE
    }

    public IsbnSource(String isbn) {
        this.isbn = isbn;
    }
}
