package com.mappers;

import java.time.LocalDateTime;

import org.mapstruct.Mapper;

@Mapper
public interface EventMapper {

    String DATE_SEPARATOR = "-";

    String HOUR_SEPARATOR = ":";

    default EventSource mapToEventSource(NewEventDTO newEventDTO) {
        if (newEventDTO == null) {
            return null;
        }
        return EventSource.builder()
            .title(newEventDTO.getTitle())
            .eventDate(mapToLocalDateTime(newEventDTO.getEventDay(), newEventDTO.getEventHour()))
            .build();
    }

    default EditEventDTO mapToEditEvent(EventSource eventSource) {
        if (eventSource == null) {
            return null;
        }

        return EditEventDTO.builder()
            .id(eventSource.getId())
            .title(eventSource.getTitle())
            .eventDay(receiveDateString(eventSource.getEventDate()))
            .eventHour(receiveHourString(eventSource.getEventDate()))
            .build();
    }

    private String receiveHourString(LocalDateTime eventDate) {
        if (eventDate == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        if (eventDate.getHour() < 10) {
            builder.append("0");
        }
        builder.append(eventDate.getHour());
        builder.append(HOUR_SEPARATOR);
        if (eventDate.getMinute() < 10) {
            builder.append("0");
        }
        builder.append(eventDate.getMinute());
        return builder.toString();
    }

    private String receiveDateString(LocalDateTime eventDate) {
        if (eventDate == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(eventDate.getYear());
        builder.append(DATE_SEPARATOR);
        if (eventDate.getMonth().getValue() < 10) {
            builder.append("0");
        }
        builder.append(eventDate.getMonth().getValue());
        builder.append(DATE_SEPARATOR);
        if (eventDate.getDayOfMonth() < 10) {
            builder.append("0");
        }
        builder.append(eventDate.getDayOfMonth());
        return builder.toString();
    }

    private LocalDateTime mapToLocalDateTime(String date, String hourAndMinutes) {
        String[] dateArray = date.split(DATE_SEPARATOR);
        String[] hourArray = hourAndMinutes.split(HOUR_SEPARATOR);
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2].substring(0, 2));
        int hour = Integer.parseInt(hourArray[0]);
        int minutes = Integer.parseInt(hourArray[1]);
        return LocalDateTime.of(year, month, day, hour, minutes);
    }
}
