package com.mappers;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@Entity
@Table(name = "events")
class EventSource {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String title;
    private LocalDateTime eventDate;

}
