package com.mappers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/events")
@RequiredArgsConstructor
class EventController {

    private final EventService eventService;

    @PostMapping
    private ResponseEntity<EditEventDTO> saveEvent(@RequestBody NewEventDTO newEventDTO) {
        return eventService.saveEvent(newEventDTO)
            .map(event -> ResponseEntity.status(201).body(event))
            .orElseGet(() -> ResponseEntity.badRequest().body(null));
    }
}
