package com.mappers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
class EditEventDTO extends NewEventDTO {

    private int id;

    @Builder
    public EditEventDTO(String title, String eventDay, String eventHour, int id) {
        super(title, eventDay, eventHour);
        this.id = id;
    }
}
