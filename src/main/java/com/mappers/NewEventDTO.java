package com.mappers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
class NewEventDTO {

    private String title;

    private String eventDay;

    private String eventHour;

    public NewEventDTO(String title, String eventDay, String eventHour) {
        this.title = title;
        this.eventDay = eventDay;
        this.eventHour = eventHour;
    }
}
