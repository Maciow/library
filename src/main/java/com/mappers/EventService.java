package com.mappers;

import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class EventService {

    private final EventMapper eventMapper;

    private final EventRepository eventRepository;

    Optional<EditEventDTO> saveEvent(NewEventDTO newEventDTO) {
        EventSource savedEvent = eventRepository.save(eventMapper.mapToEventSource(newEventDTO));
        return Optional.ofNullable(eventMapper.mapToEditEvent(savedEvent));
    }
}
